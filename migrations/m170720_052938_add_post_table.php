<?php

use yii\db\Migration;

class m170720_052938_add_post_table extends Migration
{
    public function up()
    {
		$this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'body' => $this->text(),
            'category' => $this->integer(),
            'author' => $this->integer(),
			// forigen key to status table
            'statusId' => $this->integer(),
			// date of creation
			'created_at' => $this->dateTime(),
			// date of update
			'updated_at' => $this->dateTime(),
			// user id who created the post
			'created_by' => $this->integer(),
			// user id who updated last the post
			'updated_by' => $this->integer(),
      ]);
    }

    public function down()
    {
        $this->dropTable('post');
    }
}
