<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
		
		// add the rule
		$rule = new \app\rbac\UpdateOwnPostRule;
		$auth->add($rule);
		
        //------------------------------------------------------------------------------------ Permissions ------------------------------------------------------------------------------------//
        //-----------------------------------------------------------//
		$createUser = $auth->createPermission('createUser');
        $auth->add($createUser);
		//-----------------------------------------------------------//
		$updateUser = $auth->createPermission('updateUser');
        $auth->add($updateUser);
		//-----------------------------------------------------------//
		$deleteUser = $auth->createPermission('deleteUser');
        $auth->add($deleteUser);
		//-----------------------------------------------------------//
		$updatePost = $auth->createPermission('updatePost');
        $auth->add($updatePost);
		//-----------------------------------------------------------//
		$deletePost = $auth->createPermission('deletePost');
        $auth->add($deletePost);
		//-----------------------------------------------------------//
		$createPost = $auth->createPermission('createPost');
        $auth->add($createPost);
		//-----------------------------------------------------------//
		$updateOwnPost = $auth->createPermission('updateOwnPost');
		$updateOwnPost->ruleName = $rule->name;
		$auth->add($updateOwnPost);
		//-----------------------------------------------------------//
		$userIndex = $auth->createPermission('userIndex');
        $auth->add($userIndex);
		//-----------------------------------------------------------//
		$postIndex = $auth->createPermission('postIndex');
        $auth->add($postIndex);
		//-----------------------------------------------------------//
		$viewUser = $auth->createPermission('viewUser');
        $auth->add($viewUser);
		//-----------------------------------------------------------//
		$viewPost = $auth->createPermission('viewPost');
        $auth->add($viewPost);
		//------------------------------------------------------------------------------------ Permissions ------------------------------------------------------------------------------------//
		//------------------------------------------------------------------------------------ ROLES ------------------------------------------------------------------------------------------//
		$admin = $auth->createRole('admin');
		$auth->add($admin);
		$editor = $auth->createRole('editor');
		$auth->add($editor);
		$author = $auth->createRole('author');
		$auth->add($author);
		$new_user = $auth->createRole('new_user');
		$auth->add($new_user);
		//------------------------------------------------------------------------------------ ROLES ------------------------------------------------------------------------------------------//	
		//------------------------------------------------------------------------------------ Assign -----------------------------------------------------------------------------------------//
		$auth->addChild($admin, $createUser);
		$auth->addChild($admin, $updateUser);
		$auth->addChild($admin, $deleteUser);
		$auth->addChild($admin, $editor);
		$auth->addChild($editor, $updatePost);
		$auth->addChild($editor, $deletePost);
		$auth->addChild($editor, $author);
		$auth->addChild($updateOwnPost, $updatePost);
		$auth->addChild($author, $updateOwnPost);
		$auth->addChild($author, $createPost);
		$auth->addChild($author, $new_user);
		$auth->addChild($new_user, $userIndex);
		$auth->addChild($new_user, $postIndex);
		$auth->addChild($new_user, $viewUser);
		$auth->addChild($new_user, $viewPost);
		//------------------------------------------------------------------------------------ Assign -----------------------------------------------------------------------------------------//

		

    }
}